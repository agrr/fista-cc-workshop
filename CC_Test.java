/**
 * Pteid middleware sample code
 */

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.gov.cartaodecidadao.*;


/**
 * @author agrr
 */
public class CC_Test {

    PTEID_ReaderSet readerSet = null;
    PTEID_ReaderContext readerContext = null;
    PTEID_EIDCard idCard = null;

    //Load the native library that implements the actual methods
    static
    {
        try {
            System.loadLibrary("pteidlibj");
        }
        catch (UnsatisfiedLinkError e)
        {
            System.err.println("Native code library failed to load.\n" + e);
            System.exit(1);
        }
    }

    // Extract filename without extension
    private String basename(String fullPath) { 
        char pathSeparator = '/';
        char extensionSeparator = '.';
        int dot = fullPath.lastIndexOf(extensionSeparator);
        int sep = fullPath.lastIndexOf(pathSeparator);
        return fullPath.substring(sep + 1, dot);
    }


    private void getSmartCardReaders() {
        try {
            long nrReaders = readerSet.readerCount();
            System.out.println("Nr of card readers detected: " + nrReaders);

            for (int readerIdx = 0; readerIdx < nrReaders; readerIdx++) {
                String readerName = readerSet.getReaderName(readerIdx);
                findSmartCardReaderWithCard(readerName);
            }
        } 
        catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void findSmartCardReaderWithCard(String readerName) {
        try {
            PTEID_ReaderContext rContext = readerSet.getReaderByName(readerName);

            System.out.println("Reader: " + readerName);
            if (rContext.isCardPresent()) {
                readerContext = rContext;
            }
            System.out.println("\tCard present: " + (rContext.isCardPresent() ? "yes" : "no"));

        } 
        catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getIDCard() {
        try {
            idCard = readerContext.getEIDCard();
        } 
        catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void printCitizenID(PTEID_EId id) {
        try {
            System.out.println("\n\nCitizen identification");
            System.out.println("--------------------------------------");
            System.out.println("deliveryEntity              " + id.getIssuingEntity());
            System.out.println("country                     " + id.getCountry());
            System.out.println("documentType                " + id.getDocumentType());
            System.out.println("cardNumber                  " + id.getDocumentNumber());
            System.out.println("cardNumberPAN               " + id.getDocumentPAN());
            System.out.println("cardVersion                 " + id.getDocumentVersion());
            System.out.println("deliveryDate                " + id.getValidityBeginDate());
            System.out.println("locale                      " + id.getLocalofRequest());
            System.out.println("validityDate                " + id.getValidityEndDate());
            System.out.println("firstname                   " + id.getGivenName());
            System.out.println("surname                     " + id.getSurname());
            System.out.println("sex                         " + id.getGender());
            System.out.println("nationality                 " + id.getNationality());
            System.out.println("birthDate                   " + id.getDateOfBirth());
            System.out.println("height                      " + id.getHeight());
            System.out.println("NIC                       " + id.getCivilianIdNumber());
            System.out.println("nameFather                  " + id.getSurnameFather());
            System.out.println("firstnameFather             " + id.getGivenNameFather());
            System.out.println("nameMother                  " + id.getSurnameMother());
            System.out.println("firstnameMother             " + id.getGivenNameMother());
            System.out.println("NIF                      " + id.getTaxNo());
            System.out.println("NumSS                       " + id.getSocialSecurityNumber());
            System.out.println("NumSNS                      " + id.getHealthNumber());
            System.out.println("Accidental indications      " + id.getAccidentalIndications());
            System.out.println("MRZ1                        " + id.getMRZ1());
            System.out.println("MRZ2                        " + id.getMRZ2());
            System.out.println("MRZ3                        " + id.getMRZ3());
            System.out.println("--------------------------------------");
        } 
        catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void printCitizenPersonalNotes(PTEID_EIDCard id) {
        try {
            System.out.println("\n\nCitizen personal notes  (1000 char max)");
            System.out.println("--------------------------------------");
            System.out.println(id.readPersonalNotes());
            System.out.println("--------------------------------------");
        } 
        catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void saveCitizenPic(PTEID_Photo pic, File file) {
        FileOutputStream foutput = null;
        DataOutputStream doutput = null;
        byte[] picData;
        
        try {
            foutput = new FileOutputStream(file);
            doutput = new DataOutputStream(foutput);
            
            picData = pic.getphoto().GetBytes();
            doutput.write(picData, 0, picData.length);
            doutput.flush();
            doutput.close();
            foutput.close();
        } catch (IOException ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                foutput.close();
            } catch (IOException ex) {
                Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void signPDF(String input_pdf)
    {
        String output_file = basename(input_pdf) + "_signed.pdf";
        try {
            //Signature location in the page, the location by sector option splits the page in 12 sectors
            //starting in the top-left corner of the page
            int sig_sector = 7;
            int page = 1;
            final String location = "ISCTE/IUL, Lisboa - Portugal";
            final String reason = "Demo - workshop FISTA 2017";

            PTEID_PDFSignature pdf_sig = new PTEID_PDFSignature(input_pdf);

            idCard.SignPDF(pdf_sig, page, sig_sector, pdf_sig.isLandscapeFormat(), location, reason, output_file);  

        } catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void init() {
        try {
            PTEID_ReaderSet.initSDK();
            readerSet = PTEID_ReaderSet.instance();
        } catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void release() {
        try {
            PTEID_ReaderSet.releaseSDK();
        } catch (PTEID_Exception ex) {
            Logger.getLogger(CC_Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



    
    public void start() {
       try {   
            init();
            
            getSmartCardReaders();
            
            idCard = readerContext.getEIDCard();
            
            printCitizenID(idCard.getID());

            String photo_filename = idCard.getID().getDocumentPAN() + ".png";
            
            //getPhoto() method returns the photo converted to PNG format for ease of use
            saveCitizenPic(idCard.getID().getPhotoObj(), new File(photo_filename));
            
            printCitizenPersonalNotes(idCard);

            String PDF_INPUT = "test.pdf";
            signPDF(PDF_INPUT);
            
            release();
        }
        catch (Exception ex) {
            release();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new CC_Test().start();
    }
}
