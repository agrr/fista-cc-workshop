# Workshop FISTA 17 - Cartão de Cidadão #


Código de exemplo em Java para utilizar o Middleware do Cartão de Cidadão (OSS)

Pacotes de instalação disponíveis em: 
https://svn.gov.pt/projects/ccidadao/browser/middleware-offline/tags/builds/lastversion

```
Adicionar à CLASSPATH do projecto Java o ficheiro pteidlibJava.jar localizado em:

Windows:      C:\Program Files\Portugal Identity Card\sdk\Java
Linux/MacOS:  /usr/local/lib/pteid_jni/

Definir a variável java.library.path com a directoria onde se encontra a biblioteca nativa: pteidlibJava_Wrapper
 
```