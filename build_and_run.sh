#!/bin/bash

set -e
SDK_PATH=/usr/local/lib/pteid_jni/pteidlibj.jar
javac -cp $SDK_PATH CC_Test.java
java -cp .:$SDK_PATH -Djava.library.path=/usr/local/lib/ CC_Test
